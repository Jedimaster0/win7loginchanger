﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using Microsoft.Win32.TaskScheduler;

namespace Win7LoginChanger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        // Global Vars
        const string strBackgroundDir = "C:\\Windows\\System32\\oobe\\info\\backgrounds";
        const string strPathToPhotos = "C:\\Users\\Public\\Pictures\\LoginBackgrounds";
        const string strInstallDir = "C:\\Program Files\\Win7LoginChanger";
        const string strLogFile = "C:\\Win7LoginChanger.log";
        const string strBackgroundKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Authentication\\LogonUI\\Background";
        double textBoxHeight;
        string strCurrentRunPath = System.AppDomain.CurrentDomain.BaseDirectory;
        string strUserProfilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        string strAppFileLocation = "";
        string strLastPhotoDatFile = "";
        string strLastPhotoName = "null.jpg";
        string strCurrentPhoto = "";
        string strPhotoFileName ="";
        string strActionMessage = "";
        string[] arrFilePaths;
        string[] args = Environment.GetCommandLineArgs();
        bool booLogExists = false;
        bool booRandom = false;
        bool booQuiet = false;
        bool booInstall = false;
        bool booUninstall = false;
        int intArrayPositon = 0;


        public MainWindow()
        {
            InitializeComponent();

            string strLogText = "";
            strAppFileLocation = conjoinDirFile(strCurrentRunPath, "Win7LoginChanger.exe");
            strLastPhotoDatFile = conjoinDirFile(strBackgroundDir, "lastphoto.dat");

            //
            // Listen for CLI arguments
            //
            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "--install":
                    case "/install":
                    case "/i":
                    case "-i":
                        booInstall = true;
                        break;
                    case "--uninstall":
                    case "/uninstall":
                    case "/u":
                    case "-u":
                        booUninstall = true;
                        break;
                    case "--random":
                    case "/random":
                    case "-r":
                    case "/r":
                        booRandom = true;
                        break;
                    case "--quiet":
                    case "/quiet":
                    case "-q":
                    case "/q":
                        booQuiet = true;
                        break;
                }
            }

            //
            // Check for log file and create if necessary
            //
            if (!File.Exists(strLogFile))
            {
                createTextFile(strLogFile);

                strLogText = ("--------------------" + Environment.NewLine + DateTime.Now + Environment.NewLine + "The file '" + strLogFile + "' didn't exist and was created.");
                strActionMessage = appendToTextFile(strLogFile, strLogText);
                if (File.Exists(strLogFile) && strActionMessage == "0") booLogExists = true;
            }
            else
            {
                strLogText = ("--------------------" + Environment.NewLine + DateTime.Now);
                strActionMessage = appendToTextFile(strLogFile, strLogText);
                if (File.Exists(strLogFile) && strActionMessage == "0") booLogExists = true;
            }

            //
            // Check for system background image directory and create if necessary
            //
            if (!Directory.Exists(strBackgroundDir))
            {
                writeLog(strLogFile, strBackgroundDir, "Directory not found: ", booLogExists);

                strActionMessage = createFolder(strBackgroundDir);
                if (Directory.Exists(strBackgroundDir) && strActionMessage == "0")
                {
                    writeLog(strLogFile, strBackgroundDir, "Folder created successfully: ", booLogExists);
                }
                else
                {
                    writeLog(strLogFile, strActionMessage, "Folder creation failed: ", booLogExists);
                }
            }
            else
            {
                writeLog(strLogFile, strBackgroundDir, "Directory located successfully: ", booLogExists);
            }

            //
            // Check for background directory in public profile and create if necessary
            //
            if (!Directory.Exists(strPathToPhotos))
            {
                writeLog(strLogFile, strPathToPhotos, "Directory not found: ", booLogExists);

                strActionMessage = createFolder(strPathToPhotos);
                if (Directory.Exists(strPathToPhotos) && strActionMessage == "0")
                {
                    writeLog(strLogFile, strPathToPhotos, "Folder created successfully: ", booLogExists);
                }
                else
                {
                    writeLog(strLogFile, strActionMessage, "Folder creation failed: ", booLogExists);
                }
            }
            else
            {
                writeLog(strLogFile, strPathToPhotos, "Directory located successfully: ", booLogExists);
            }

            //
            // Create create and/or change the registry key as necessary
            //
            strActionMessage = changeHKLMRegValue(strBackgroundKey, "OEMBackground", "1");
            writeLog(strLogFile, strActionMessage, "Registry entry status: ", booLogExists);


            //
            // Process CLI arguments
            //
            if (booInstall)
            {
                install();
                writeLog(strLogFile, "Installing and creating scheduled task.", "Install switch detected: ", booLogExists);
            }
            if (booUninstall)
            {
                uninstall();
                writeLog(strLogFile, "Uninstalling and removing scheduled task.", "Uninstall switch detected: ", booLogExists);
                Application.Current.Shutdown();
            }
            if (booQuiet)
            {
                getPhoto();
                nextPhoto();
                setPhoto();
                Application.Current.Shutdown();
            }
        }

        //
        // This method runs only once the GUI is loaded
        //
        private void Window_Loaded(object sender, EventArgs eventArgs)
        {
            textBoxHeight = txt_status.ActualHeight;
            watch();
            getPhoto();
            nextPhoto();
            if (booRandom) { chk_random.IsChecked = true; }
        }


        //
        // Functions
        //
        private void getPhoto()
        {

            try { 
                if (Directory.Exists(strPathToPhotos))
                {
                    arrFilePaths = Directory.GetFiles(strPathToPhotos, "*.jpg"); // Build an array containing the .jpg files in the user's background directory

                    if (arrFilePaths.Length < 1)
                    {
                        writeLog(strLogFile, "No photos found. Check file/folder permissions.", "Error parsing photos: ", booLogExists); return;
                    }
                    else
                    {
                        if (File.Exists(strLastPhotoDatFile) && readDatFile(strLastPhotoDatFile) != "unknown")
                        {
                            intArrayPositon = Array.FindIndex(arrFilePaths, item => item.Contains(readDatFile(strLastPhotoDatFile))) + 1;
                            strLastPhotoName = readDatFile(strLastPhotoDatFile);
                            writeLog(strLogFile, strLastPhotoName, "Previous set photo found: ", booLogExists);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                writeLog(strLogFile, e.Message, "Error obtaining photos: ", booLogExists);
            }
        }

        private void nextPhoto()
        {
            if (arrFilePaths.Length > 0)
            {
                Random random = new Random();
                int intRandomNumber = random.Next(0, arrFilePaths.Length) + 1;

                if (intArrayPositon < arrFilePaths.Length) { intArrayPositon++; } else { intArrayPositon = 1; }
                if (booRandom == true) { intArrayPositon = intRandomNumber; }

                strPhotoFileName = System.IO.Path.GetFileName(arrFilePaths[intArrayPositon - 1]);
                strCurrentPhoto = conjoinDirFile(strPathToPhotos, strPhotoFileName);
                Application.Current.Dispatcher.Invoke(() => {
                    txt_currentPhoto.Text = (intArrayPositon) + "/" + arrFilePaths.Length;
                    img_ImageViewer.Source = new BitmapImage(new Uri(strCurrentPhoto));
                });
            }
            else
            {
                writeLog(strLogFile, "No photos found.", "Error loading photo: ", booLogExists);
            }
        }

        private void previousPhoto()
        {
            if (arrFilePaths.Length > 0)
            {
                if ((intArrayPositon - 1) > 0) { intArrayPositon--; } else { intArrayPositon = arrFilePaths.Length; }

                strPhotoFileName = System.IO.Path.GetFileName(arrFilePaths[intArrayPositon - 1]);
                strCurrentPhoto = conjoinDirFile(strPathToPhotos, strPhotoFileName);
                Application.Current.Dispatcher.Invoke(() => {
                    txt_currentPhoto.Text = (intArrayPositon) + "/" + arrFilePaths.Length;
                    img_ImageViewer.Source = new BitmapImage(new Uri(strCurrentPhoto));
                });
            }
            else
            {
                writeLog(strLogFile, "No photos found.", "Error loading photo: ", booLogExists);
            }
        }

        private void setPhoto()
        {
            string strBackgroundDefaultFile = conjoinDirFile(strBackgroundDir, "backgroundDefault.jpg");

            if (arrFilePaths.Length > 1 && strPhotoFileName == strLastPhotoName)
            {
                nextPhoto();
                writeLog(strLogFile, "Existing file and current file are the same, loading next image.", "File conflict: ", booLogExists);
            } else
            {
                strActionMessage = copyFile(strCurrentPhoto, strBackgroundDefaultFile, true);
                if (strActionMessage == "0" && File.Exists(strBackgroundDefaultFile))
                {
                    strLastPhotoName = strPhotoFileName;
                    writeToTextFile(strLastPhotoDatFile, strPhotoFileName);
                    writeLog(strLogFile, "Using photo: " + (intArrayPositon) + ", out of: " + arrFilePaths.Length, "Photos parsed: ", booLogExists);
                    writeLog(strLogFile, strBackgroundDefaultFile, "File copied successfully: ", booLogExists);
                }
                else
                {
                    writeLog(strLogFile, strActionMessage, "File copy failed: ", booLogExists);
                }
            }
        }

        private void install()
        {
            //
            // Create scheduled task
            //
            try
            {
                using (TaskService ts = new TaskService())
                {
                    TaskDefinition td = ts.NewTask();
                    td.Principal.RunLevel = TaskRunLevel.Highest;
                    td.RegistrationInfo.Description = "Changes Windows 7 login background when logging in or unlocking the machine.";
                    td.Settings.MultipleInstances = TaskInstancesPolicy.StopExisting;
                    td.Settings.ExecutionTimeLimit = TimeSpan.FromSeconds(60);
                    td.Settings.StartWhenAvailable = true;
                    td.Settings.DisallowStartIfOnBatteries = false;
                    td.Settings.StopIfGoingOnBatteries = false;

                    LogonTrigger lt = new LogonTrigger();
                    SessionStateChangeTrigger sTrigger = new SessionStateChangeTrigger { StateChange = TaskSessionStateChangeType.SessionUnlock };

                    td.Triggers.Add(lt);
                    td.Triggers.Add(sTrigger);

                    td.Actions.Add(new ExecAction(strAppFileLocation, "--quiet --random"));

                    ts.RootFolder.RegisterTaskDefinition("Win7LoginChanger", td, TaskCreation.CreateOrUpdate, "SYSTEM", null, TaskLogonType.ServiceAccount);

                    writeLog(strLogFile, "Win7LoginChanger", "Created scheduled task: ", booLogExists);
                }
            }
            catch (Exception e)
            {
                writeLog(strLogFile, e.Message, "Error creating scheduled task: ", booLogExists);
            }
        }

        private void uninstall()
        {
            //
            // Remove scheduled task
            //
            try
            {
                using (TaskService ts = new TaskService())
                {
                    ts.RootFolder.DeleteTask("Win7LoginChanger", false);
                }
                writeLog(strLogFile, "Win7LoginChanger", "Removed scheduled task: ", booLogExists);
            }
            catch (Exception e) { writeLog(strLogFile, e.Message, "Error removing scheduled task: ", booLogExists); }
        }

        private void writeLog(string strTextFile, string strMessageText, string strTitle, bool booTextFileExists)
        {
            if (booTextFileExists) { appendToTextFile(strTextFile, strTitle + strMessageText); }
            Application.Current.Dispatcher.Invoke(() => {
                txt_status.AppendText(Environment.NewLine + DateTime.Now + ": " + strTitle + strMessageText);
            });
        }

        private static string conjoinDirFile(string directory, string fileName)
        {
            return System.IO.Path.Combine(directory, fileName);
        }

        private static string createFolder(string strFolder)
        {
            try { Directory.CreateDirectory(strFolder); }
            catch (Exception e) { return e.Message; }
            return "0";
        }

        private static string deleteFolder(string strFolder, bool booRecursive)
        {
            try { Directory.Delete(strFolder, booRecursive); }
            catch (Exception e) { return e.Message; }
            return "0";
        }

        private static string readDatFile(string datFile)
        {
            try {
                string textData = File.ReadAllText(datFile);
                return textData;
            }
            catch (Exception e) { return "Error reading file: " + e.Message; }
        }

        private static string copyFile(string strSourceFile, string strDestinationFile, bool booOverWrite)
        {
            try { File.Delete(strDestinationFile); File.Copy(strSourceFile, strDestinationFile, booOverWrite); }
            catch (Exception e) { return e.Message; }
            return "0";
        }

        private static string copyAppFiles(string strSourceDir, string strDestinationDir, bool booOverWrite)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(strSourceDir);

                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string temppath = System.IO.Path.Combine(strDestinationDir, file.Name);
                    if (file.Name.EndsWith(".exe") || file.Name.EndsWith(".dll")) file.CopyTo(temppath, booOverWrite);
                }
                return "0";
            }
            catch (Exception e) { return e.Message; }
        }

        private static string createTextFile(string strFile)
        {
            try { using (var myFile = File.CreateText(strFile)) { } }
            catch (Exception e) { return e.Message; }
            return "0";
        }

        private static string writeToTextFile(string strTextFile, string strLinesToWrite)
        {
            try { File.WriteAllText(strTextFile, strLinesToWrite); return "0"; }
            catch (Exception e) { return e.Message; }
        }

        private static string appendToTextFile(string strTextFile, string strText)
        {
            if (File.Exists(strTextFile))
            {
                try
                {
                    using (StreamWriter sw = File.AppendText(strTextFile))
                    {
                        sw.WriteLine(strText);
                    }
                }
                catch (Exception e) { return e.Message; }
            }
            return "0";
        }

        private string changeHKLMRegValue(string strRegKey, string strRegValue, string strRegValueData)
        {
            try
            {
                RegistryKey objRK = Registry.LocalMachine.OpenSubKey(strRegKey, true);
                if (objRK.GetValue(strRegValue).ToString() == strRegValueData)
                {
                    objRK.Close();
                    return "Registry value already contains the correct data. No modification necessary.";
                }
                else
                {
                    objRK.SetValue(strRegValue, strRegValueData, RegistryValueKind.DWord);
                    string strRegTest = objRK.GetValue(strRegValue).ToString();
                    objRK.Close();
                    if (strRegTest == strRegValueData)
                    {
                        return "Registry value modified successfully.";
                    }
                    else
                    {
                        return "Value could not be modified. Ensure that you have permission to modify the registry.";
                    }
                }
            }
            catch (Exception e) { return e.Message; }
        }

        private static bool isFileReady(String sFilename)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(sFilename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    if (inputStream.Length > 0) { return true; }
                    else { return false; }
                }
            }
            catch (Exception) { return false; }
        }

        private void watch()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = strPathToPhotos;
            watcher.NotifyFilter =  NotifyFilters.LastWrite |
                                    NotifyFilters.FileName |
                                    NotifyFilters.DirectoryName |
                                    NotifyFilters.CreationTime |
                                    NotifyFilters.Size;
            watcher.Filter = "*.*";
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            getPhoto();
            nextPhoto();
            writeLog(strLogFile, "Reloading file index.", "File/Directory changes detected: ", booLogExists);
        }

        private void btn_apply_Click(object sender, RoutedEventArgs e)
        {
            setPhoto();
        }

        private void btn_previous_Click(object sender, RoutedEventArgs e)
        {
            previousPhoto();
        }

        private void btn_next_Click(object sender, RoutedEventArgs e)
        {
            nextPhoto();
        }

        private void txt_status_MouseEnter(object sender, MouseEventArgs e)
        {
            if (txt_status.Text.Length > 0) { txt_status.Height = double.NaN; }
        }

        private void txt_status_MouseLeave(object sender, MouseEventArgs e)
        {
            txt_status.Height = textBoxHeight;
        }

        private void btn_install_Click(object sender, RoutedEventArgs e)
        {
            install();
        }

        private void btn_uninstall_Click(object sender, RoutedEventArgs e)
        {
            uninstall();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            booRandom = true;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            booRandom = false;
        }

        private void cm_OpenPhotoLocation_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(strCurrentPhoto))
            {
                System.Diagnostics.Process.Start("explorer.exe", "/select, \"" + strCurrentPhoto + "\"");
            }
        }

        private void cm_openBgDir_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(strBackgroundDir))
            {
                System.Diagnostics.Process.Start("explorer.exe", "\"" + strBackgroundDir + "\"");
            }
        }

        private void cm_openPhotoDir_Click(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(strPathToPhotos))
            {
                System.Diagnostics.Process.Start("explorer.exe", "\"" + strPathToPhotos + "\"");
            }
        }

        private void cm_refresh_Click(object sender, RoutedEventArgs e)
        {
            getPhoto();
            nextPhoto();
        }
    }
}
